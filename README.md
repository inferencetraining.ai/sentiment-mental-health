# Sentiment Mental Health



## Getting started

Building an app in frontend flutter and with Python AWS EC2 backend
Run the model with Transformers, Scipy and Flask



## Collaborate with your team

Use taiga.io and gitlab to colloborate 
## Test and Deploy

Test and deploy on AWS, flutter and gitlab



## Name

TBA

## Description
Assesing mental health status with an AI algorithm


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
email emmanuel@inferencetraining.ai

## Roadmap
We hope to deploy this in the market - you can follow the roadman on here https://tree.taiga.io/project/inferencetrainingai-sentiment-mental-health-ai/kanban

## Contributing
We contrbutnig AI to the psychology field, namely sentimnt analysis
## Authors and acknowledgment
 - Asad Ali Khan helped with developing the Machine Intelligence model - measuring performance
## License
MIT license

## Project status
Still in it's infancy 