from barchart import barchart 
from upload_file import upload_file
#import email_file_link
from db_store import db
import requests
import numpy as np

def run(query):
    sentiment = requests.post('', body={'text_input': query})
    dates = []
    data = []
    dbstore = db(sentiment)
    for i in dbstore[1]:
        dates.append(i)
        data.append(dbstore[0][i])
    
    for d, i in enumerate(data):
        data[d] = np.mean(i)


    datesplit = []
    for x in dates:
        datesplit.append(x.split('-')[2])
    
    chart = barchart(datesplit, data)
    upload = upload_file(chart)
    #email_file_link(upload_file)


