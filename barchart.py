import matplotlib.pyplot as plt
import numpy as np



def barchart(dates, data):
    #bar chart of positivity]
    pos = np.arange(len(dates))
    plt.bar(pos,data,color='blue',edgecolor='black')
    plt.xticks(pos, dates)
    plt.xlabel('Days', fontsize=16)
    plt.ylabel('Positivity', fontsize=16)
    plt.title('Positivity over days',fontsize=20)
    plt.savefig('figure7.png')
    plt.clf()
    return('figure7.png')