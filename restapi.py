from flask import Flask, redirect, url_for, request
import sentiment
from run import run

app = Flask(__name__)
#effective rest api
@app.route("/", methods = ['POST', 'GET'])
def mood_sentiment():
   if request.method == 'POST':
    data = request.form
    run.run(data['query'])